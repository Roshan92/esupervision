<?php
// error_reporting(E_ALL & ~E_NOTICE);
//checks if the given username exists in the database
function user_exists($username){
	//sql injection prevention
	$username = mysql_real_escape_string($username);

	return (mysql_result(mysql_query("SELECT COUNT(`id`)
									FROM `users`
									WHERE `username`='$username'"), 0) == 1) ? true : false;
}

//checks if the given username and password combination is valid
function validation($username, $password){
	$username = mysql_real_escape_string($username);
	$password = mysql_real_escape_string($password);

	return (mysql_result(mysql_query("SELECT COUNT(`id`)
									FROM `users`
									WHERE `username`='$username'
									AND `password`='$password'"), 0) == 1) ? true : false;
}

//checks if a given user account is active or not in login.html
function is_actived($username){
	$username = mysql_real_escape_string($username);

	return (mysql_fetch_row(mysql_query(
						"SELECT * FROM `users`
						INNER JOIN `user_activations`
						ON `users`.`id` = `user_activations`.`user_id`
						WHERE `users`.`username` = '{$username}'
						AND `user_activations`.`is_active` = 1 "), 0));

}

//checks if a given user account is active or not in activate.html
function is_active($username, $activation_code){
	$username = mysql_real_escape_string($username);

	return (mysql_fetch_row(mysql_query(
						"SELECT * FROM `users`
						INNER JOIN `user_activations`
						ON `users`.`id` = `user_activations`.`user_id`
						WHERE `users`.`username` = '{$username}'
						AND `user_activations`.`activation_code` = '{$activation_code}'"), 0));
}

// get id from username
function get_user_id($username){

	$sql = mysql_result(mysql_query("SELECT `id`
		   							FROM `users`
		   							WHERE `users`.`username` = '{$username}'"),0);
	return $sql;
}

// activates the user related to the given activation code
function activate_users($activation_code, $username){
	$activation_code = mysql_real_escape_string($activation_code);

	$user_id = get_user_id($username);
	$message = "Your account has been successfully activated.";
	$log = "You activated your account";

	mysql_query("UPDATE `user_activations` SET `is_active` = '1' WHERE `activation_code` = '{$activation_code}'");

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message . "', '" . $user_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $user_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

}

//get role by priority
function get_name_by_priority($priority){
	$sql = mysql_result(mysql_query("SELECT `name`
									FROM `roles`
									WHERE `priority` = '{$priority}'"),0);
	return $sql;
}

//adds a new user to a database
function add_user($name, $username, $email, $password, $priority, $interest_id, $project_title){
	//XSS attack is prevented by using htmlentities
	$username = mysql_real_escape_string(htmlentities($username));
	$email = mysql_real_escape_string($email);
	$password_real = $password;
	$password = sha1($password);

	$charset = array_flip(array_merge(range('a','z'), range('A','Z'), range(0, 9)));
	$activation_code = implode('', array_rand($charset, 5));

	$body = <<<EMAIL

	Welcome to eSupervision !

	You have been registered by course coordinator, you must activate your account before login.

	Login credentials : -

	Username : {$username}
	Pasword : {$password_real}

	Here is the activation code : {$activation_code}

	Please enter the code above in the activation page.

	Thank you.

EMAIL;

	mail($email, 'Welcome to eSupervision', $body, 'From: icemission@gmail.com');

	$sql = "INSERT INTO `users` (`name`,`username`,`password`,`email`,`priority`) VALUES ('" . $name . "', '" . $username . "', '" . $password ."', '" . $email . "', '" . $priority . "')";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$user_id = mysql_insert_id();

	foreach($interest_id as $interest_id) {
		$sql = "INSERT INTO `users_interests` (`user_id`,`interest_id`) VALUES ('" . $user_id . "', '" . $interest_id ."')";
		mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
	}

	$sql = "INSERT INTO `student_project` (`project_title`,`student_id`) VALUES ('" . $project_title . "', '" . $user_id ."')";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `user_activations` (`user_id`,`activation_code`) VALUES ('" . $user_id . "', '" . $activation_code ."')";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	if ($priority == 3){
		$sql = "INSERT INTO `allocation` (`student_id`) VALUES ('" . $user_id . "')";
	  	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
	}

	//log
	// $role = get_name_by_priority($priority);
	// $log = "You add a new {$role} by the name of {$name}";

	// $sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $user_id . "', NOW())";
	// mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
}


// add roles
function add_roles($name, $priority, $username){
	$name = mysql_real_escape_string($name);

	$sql = "INSERT INTO `roles` (`name`, `priority`) VALUES ('" . $name . "', '" . $priority . "')";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	//log
	$user_id = get_user_id($username);
	$log = "You added a new role, {$name}";

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $user_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

}

// retrieves all roles
function get_roles(){

	$sql = "SELECT `roles`.`priority` AS `priority`,
		   `roles`.`name` AS `name`
		   FROM `roles`
		   ORDER BY `priority` ASC";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'priority'	=> $row['priority'],
			'name'	=> $row['name'],
		);
	}

	return $rows;
}

// get students
function get_students(){

	$sql = "SELECT *
		   FROM `users`
		   WHERE `users`.`priority` = 3";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'student_id'	=> $row['id'],
			'name'	=> $row['name'],
			'email' => $row['email'],
			'username' => $row['username']
		);
	}

	return $rows;

}

//get staff
function get_staffs(){

	$sql = "SELECT *
		   FROM `users`
		   WHERE `users`.`priority` = 2";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'staff_id'	=> $row['id'],
			'name'	=> $row['name'],
		);
	}

	return $rows;

}


//get messages by id
function get_student_by_id($student_id){
	$student_id = (int)$student_id;

	$sql = "SELECT `supervisor`, `second_marker`
			FROM `dashboard`
			WHERE `student_id`='$student_id'";

	$message = mysql_fetch_assoc(mysql_query($sql),0);

	return $message;
}

//get email from using user_id
function get_email($user_id){

	$sql = mysql_result(mysql_query("SELECT `users`.`email`
									FROM `users`
									WHERE `users`.`id` = '{$user_id}'"),0);

	return $sql;
}

//get name from using user_id supervisor and second marker
function get_name($staff_id){

	$sql = mysql_result(mysql_query("SELECT `users`.`name`
									FROM `users`
									WHERE `users`.`id` = '{$staff_id}'"),0);

	return $sql;
}

//checks for duplication in allocation table
function allocation_exists($student){

	return (mysql_result(mysql_query("SELECT count(`id`)
									FROM `allocation`
									WHERE `student_id`='{$student}'"), 0) == 1) ? true : false;
}

//checks for allocation in supervisor
function allocation_exist_supervisor($student){

// 	$sql = mysql_result(mysql_query("SELECT `supervisor_id`
// 									FROM `allocation`
// 									WHERE `student_id`='$student'"),0);
// var_dump($sql);
// die();

	// $student = (int)$student;

	// $sql = "SELECT `supervisor_id`
 // 			FROM `allocation`
 // 			WHERE `student_id`='$student'";

	// $allocation = mysql_fetch_assoc(mysql_query($sql),0);

	// return $allocation;
	// return $sql;

	$sql = mysql_fetch_row(mysql_query("SELECT `supervisor_id`
										FROM `allocation`
										WHERE `student_id`='{$student}'"), 0);

	var_dump($sql);
	die();



	// return (mysql_result(mysql_query("SELECT COUNT(`id`)
	// 								FROM `allocation`
	// 								WHERE `student_id`='$student'
	// 								AND `supervisor_id`='$supervisor'"), 0) == 1) ? true : false;
}

//checks for allocation in second marker
function allocation_exist_second_marker($student){

	// $sql = mysql_result(mysql_query("SELECT `second_marker_id`
	// 								FROM `allocation`
	// 								WHERE `student_id`='$student'"),0);

	// return $sql;

	return (mysql_fetch_row(mysql_query("SELECT `second_marker_id`
										FROM `allocation`
										WHERE `student_id`='{$student}'"), 0));

	// return (mysql_result(mysql_query("SELECT COUNT(`id`)
	// 								FROM `allocation`
	// 								WHERE `student_id`='$student'
	// 								AND `second_marker_id`='$second_marker'"), 0) == 1) ? true : false;
}

//bulk allocation
function bulk_allocate($student, $supervisor, $second_marker, $username){
	$student_name = get_name($student);
	$supervisor_name = get_name($supervisor);
	$second_marker_name = get_name($second_marker);

	foreach($student as $student)
	{
	  // $sql = "INSERT INTO `allocation` (`student_id`, `supervisor_id`, `second_marker_id`) VALUES ('" . $student . "', '" . $supervisor . "', '" . $second_marker . "')";
	  // mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
		mysql_query("INSERT INTO `allocation`
					SET `supervisor_id` = '{$supervisor}',
					`second_marker_id` = '{$second_marker}'
					WHERE `student_id` = '{$student}'");


	$body = <<<EMAIL

	Welcome to eSupervision !

	You supervisor is {$supervisor_name} and your second marker is {$second_marker_name}.

	Thank you.

EMAIL;

	mail(get_email($student), 'Welcome to eSupervision', $body, 'From: admin@esupervision.com');

	$admin = get_user_id($username);

	$log = "You allocated {$student_name} to supervisor, {$supervisor_name} and second marker, {$second_marker_name}";

	$message_student = "You have been allocated to supervisor, {$supervisor_name} and second marker, {$second_marker_name}";

	$message_supervisor= "You have been allocated to be the supervisor of {$student_name}";

	$message_second_marker= "You have been allocated to be the second marker of {$student_name}";


	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_student . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_supervisor . "', '" . $supervisor . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_second_marker . "', '" . $second_marker . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $admin . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
	}
}

//bulk allocation supervisor
function bulk_allocate_supervisor($student, $supervisor, $username){
	$student_name = get_name($student);
	$supervisor_name = get_name($supervisor);

	foreach($student as $student)
	{
	  $sql = "INSERT INTO `allocation` (`student_id`, `supervisor_id`) VALUES ('" . $student . "', '" . $supervisor . "')";
	  mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());


	$body = <<<EMAIL

	Welcome to eSupervision !

	You supervisor is {$supervisor_name}.

	Thank you.

EMAIL;

	mail(get_email($student), 'Welcome to eSupervision', $body, 'From: admin@esupervision.com');

	$admin = get_user_id($username);

	$log = "You allocated {$student_name} to supervisor, {$supervisor_name}";

	$message_student = "You have been allocated to supervisor, {$supervisor_name}";

	$message_supervisor= "You have been allocated to be the supervisor of {$student_name}";

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_student . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_supervisor . "', '" . $supervisor . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $admin . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
	}
}


//bulk allocation second marker
function bulk_allocate_second_marker($student, $second_marker, $username){
	$student_name = get_name($student);
	$second_marker_name = get_name($second_marker);

	foreach($student as $student)
	{
	  $sql = "INSERT INTO `allocation` (`student_id`, `second_marker_id`) VALUES ('" . $student . "', '" . $second_marker . "')";
	  mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());


	$body = <<<EMAIL

	Welcome to eSupervision !

	You second marker is {$second_marker_name}.

	Thank you.

EMAIL;

	mail(get_email($student), 'Welcome to eSupervision', $body, 'From: admin@esupervision.com');

	$admin = get_user_id($username);

	$log = "You allocated {$student_name} to second marker, {$second_marker_name}";

	$message_student = "You have been allocated to second marker, {$second_marker_name}";

	$message_second_marker= "You have been allocated to be the second marker of {$student_name}";


	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_student . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_second_marker . "', '" . $second_marker . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $admin . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
	}
}


//update bulk allocation
function update_bulk_allocate($student, $supervisor, $second_marker, $username){
	$supervisor_name = get_name($supervisor);
	$second_marker_name = get_name($second_marker);

	foreach($student as $student)
	{
		$student_name = get_name($student);

	  	mysql_query("UPDATE `allocation`
					SET `supervisor_id` = '{$supervisor}',
					`second_marker_id` = '{$second_marker}'
					WHERE `student_id` = '{$student}'");


	$body = <<<EMAIL

	Welcome to eSupervision !

	Your supervisor is {$supervisor_name} and your second marker is {$second_marker_name}.

	Thank you.

EMAIL;

	mail(get_email($student), 'Welcome to eSupervision', $body, 'From: admin@esupervision.com');

	$admin = get_user_id($username);

	$log = "You assigned {$student_name} to supervisor, {$supervisor_name} and second marker, {$second_marker_name}";

	$message_student = "You have been assigned to supervisor, {$supervisor_name} and second marker, {$second_marker_name}";

	$message_supervisor= "You have been assigned to be the supervisor of {$student_name}";

	$message_second_marker= "You have been assigned to be the second marker of {$student_name}";


	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_student . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_supervisor . "', '" . $supervisor . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_second_marker . "', '" . $second_marker . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $admin . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	}

}


//update bulk allocate supervisor
function update_bulk_allocate_supervisor($student, $supervisor, $username){
	$supervisor_name = get_name($supervisor);

	foreach($student as $student)
	{
		$student_name = get_name($student);

	  	mysql_query("UPDATE `allocation`
					SET `supervisor_id` = '{$supervisor}'
					WHERE `student_id` = '{$student}'");


	$body = <<<EMAIL

	Welcome to eSupervision !

	Your new supervisor is {$supervisor_name}.

	Thank you.

EMAIL;

	mail(get_email($student), 'Welcome to eSupervision', $body, 'From: admin@esupervision.com');

	$admin = get_user_id($username);

	$log = "You assigned {$student_name} to supervisor, {$supervisor_name}";

	$message_student = "You have been assigned to supervisor, {$supervisor_name}";

	$message_supervisor= "You have been assigned to be the supervisor of {$student_name}";

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_student . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_supervisor . "', '" . $supervisor . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $admin . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	}

}


//update bulk allocate second marker
function update_bulk_allocate_second_marker($student, $second_marker, $username){
	$second_marker_name = get_name($second_marker);

	foreach($student as $student)
	{
		$student_name = get_name($student);

	  	mysql_query("UPDATE `allocation`
					SET `second_marker_id` = '{$second_marker}'
					WHERE `student_id` = '{$student}'");


	$body = <<<EMAIL

	Welcome to eSupervision !

	Your new second marker is {$second_marker_name}.

	Thank you.

EMAIL;

	mail(get_email($student), 'Welcome to eSupervision', $body, 'From: admin@esupervision.com');

	$admin = get_user_id($username);

	$log = "You assigned {$student_name} to second marker, {$second_marker_name}";

	$message_student = "You have been assigned to second marker, {$second_marker_name}";

	$message_second_marker= "You have been assigned to be the second marker of {$student_name}";


	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_student . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_second_marker . "', '" . $second_marker . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $admin . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	}

}


//gets supervisor by student
function get_supervisor_by_student($student){

	$sql = mysql_result(mysql_query("SELECT * FROM `users`
								   INNER JOIN `allocation`
								   ON `users`.`id` = `allocation`.`supervisor_id`
								   WHERE `allocation`.`student_id` = '{$student}'"),0);

	return $sql;
}

//gets second marker by student
function get_second_marker_by_student($student){

	$sql = mysql_result(mysql_query("SELECT * FROM `users`
								   INNER JOIN `allocation`
								   ON `users`.`id` = `allocation`.`second_marker_id`
								   WHERE `allocation`.`student_id` = '{$student}'"),0);

	return $sql;
}

//gets supervisor from allocation
function get_supervisor_allocation($student){

	$sql = "SELECT * FROM `users`
		   INNER JOIN `allocation`
		   ON `users`.`id` = `allocation`.`supervisor_id`
		   WHERE `allocation`.`student_id` = '{$student}' ";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'supervisor_id'	=> $row['id'],
			'name'	=> $row['name'],
		);
	}

	return $rows;

}


//gets second marker from allocation
function get_second_marker_allocation($student){

	$sql = "SELECT * FROM `users`
		   INNER JOIN `allocation`
		   ON `users`.`id` = `allocation`.`second_marker_id`
		   WHERE `allocation`.`student_id` = '{$student}' ";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'second_marker_id'	=> $row['id'],
			'name'	=> $row['name'],
		);
	}

	return $rows;

}

//gets supervisor from allocation
function get_staff_allocation($student){

	$sql = "SELECT `users`.`id`, `users`.`name` FROM `users`
		   INNER JOIN `allocation`
		   ON `users`.`id` = `allocation`.`supervisor_id` OR `users`.`id` = `allocation`.`second_marker_id`
		   WHERE `allocation`.`student_id` = '{$student}' ";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'staff_id'	=> $row['id'],
			'name'	=> $row['name'],
		);
	}

	return $rows;

}

// add dashboard
function dashboard($supervisor, $second_marker, $student){

	$sql = "INSERT INTO `dashboard` (`supervisor`,`second_marker`,`student_id`) VALUES ('" . $supervisor . "', '" . $second_marker . "', '" . $student . "')";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$supervisor_id = get_supervisor_by_student($student);

	$second_marker_id = get_second_marker_by_student($student);

	$student_name = get_name($student);

	$log = "You added your interaction with supervisor and second marker on your dashboard";

	$message_supervisor= "Your student, {$student_name} have added a brief interaction with you on the dashboard";

	$message_second_marker= "Your student, {$student_name} have added a brief interaction with you on the dashboard";


	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_supervisor . "', '" . $supervisor_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_second_marker . "', '" . $second_marker_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

}

//updates dashboard supervisor
function update_dashboard_supervisor($supervisor, $student){

	mysql_query("UPDATE `dashboard`
				SET `supervisor` = '{$supervisor}'
				WHERE `student_id` = '{$student}'");

	$supervisor_id = get_supervisor_by_student($student);

	$student_name = get_name($student);

	$log = "You updated your interaction with supervisor on your dashboard";

	$message_supervisor= "Your student, {$student_name} have updated a brief interaction with you on the dashboard";

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_supervisor . "', '" . $supervisor_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

}


//updates dashboard second marker
function update_dashboard_second_marker($second_marker, $student){

	mysql_query("UPDATE `dashboard`
				SET `second_marker` = '{$second_marker}'
				WHERE `student_id` = '{$student}'");

	$second_marker_id = get_second_marker_by_student($student);

	$student_name = get_name($student);

	$log = "You updated your interaction with second marker on your dashboard";

	$message_second_marker= "Your student, {$student_name} have updated a brief interaction with you on the dashboard";

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message_second_marker . "', '" . $second_marker_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $student . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

}


//checks for duplication in dashboard table
function dashboard_exists($student){

	return (mysql_result(mysql_query("SELECT count(`id`) FROM `dashboard` WHERE `student_id`='$student'"), 0) == 1) ? true : false;
}



// get dashboard by username
function get_user_dashboard($username){

	$sql = "SELECT `supervisor`, `second_marker`
			FROM `dashboard`
			INNER JOIN `users`
			ON `dashboard`.`student_id` = `users`.`id`
			WHERE `users`.`username` = '{$username}'";

	$dashboard = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($dashboard)){
		$rows[] = array(
			'supervisor'	=> $row['supervisor'],
			'second_marker'	=> $row['second_marker'],
		);
	}

	return $rows;
}


//get roles by username
function get_roles_by_username($username){

	$sql = mysql_result(mysql_query("SELECT `priority`
									FROM `users`
									WHERE `username` = '{$username}'"),0);

	return $sql;
}



//staff see student's as per allocation
function get_student_per_staff_allocation($user_id){

	$sql = "SELECT `users`.`id`,`users`.`name` AS 'student_name'
			FROM (users
			INNER JOIN allocation
			ON `users`.`id` = `allocation`.`student_id`)
			WHERE `allocation`.`supervisor_id` = '{$user_id}' OR `allocation`.`second_marker_id` = '{$user_id}'";

	$dashboard = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($dashboard)){
		$rows[] = array(
			'id' => $row['id'],
			'student'	=> $row['student_name']
		);
	}

	return $rows;
}


//staff see student's as per allocation
function get_student_per_supervisor_allocation($user_id){

	$sql = "SELECT `users`.`id`,`users`.`name` AS 'student_name'
			FROM (users
			INNER JOIN allocation
			ON `users`.`id` = `allocation`.`student_id`)
			WHERE `allocation`.`supervisor_id` = '{$user_id}'";

	$dashboard = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($dashboard)){
		$rows[] = array(
			'id' => $row['id'],
			'student'	=> $row['student_name']
		);
	}

	return $rows;
}

//count student per supervisor
function get_student_per_supervisor_allocation_count($user_id){

	$sql = mysql_result(mysql_query("SELECT COUNT(name)
									FROM (users
									INNER JOIN allocation
									ON `users`.`id` = `allocation`.`student_id`)
									WHERE `allocation`.`supervisor_id` = '{$user_id}'"),0);

	return $sql;
}

//count student per second marker
function get_student_per_second_marker_allocation_count($user_id){

	$sql = mysql_result(mysql_query("SELECT COUNT(name)
									FROM (users
									INNER JOIN allocation
									ON `users`.`id` = `allocation`.`student_id`)
									WHERE `allocation`.`second_marker_id` = '{$user_id}'"),0);

	return $sql;
}


//supervisor see student's as per allocation
function get_student_per_second_marker_allocation($user_id){

	$sql = "SELECT `users`.`id`,`users`.`name` AS 'student_name'
			FROM (users
			INNER JOIN allocation
			ON `users`.`id` = `allocation`.`student_id`)
			WHERE `allocation`.`second_marker_id` = '{$user_id}'";

	$dashboard = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($dashboard)){
		$rows[] = array(
			'id' => $row['id'],
			'student'	=> $row['student_name']
		);
	}

	return $rows;
}

//new message
function new_message($sender_id, $receiver_id, $message){

	$sql = "INSERT INTO `messages` (`receiver_id`,`sender_id`,`message`,`sent_on`) VALUES ('" . $receiver_id . "', '" . $sender_id . "', '" . $message . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sender_name = get_name($sender_id);
	$receiver_name = get_name($receiver_id);

	$log = "You sent a message to $receiver_name";

	$message = "$sender_name sent you a message";

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message . "', '" . $receiver_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $sender_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
}

//gets messages per user_id
function get_new_message($username){

	$sql = "SELECT `messages`.`id`, `messages`.`sender_id`, `messages`.`receiver_id`, `messages`.`message`
			FROM `messages`
			INNER JOIN `users`
			ON `messages`.`receiver_id` = `users`.`id`
			WHERE `users`.`username` = '{$username}'
			ORDER BY `messages`.`sent_on` DESC";

	$messages = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($messages)){
		$rows[] = array(
			'message_id' => $row['id'],
			'message' => $row['message'],
			'sender' => get_name($row['sender_id'])
		);
	}

	return $rows;
}

//gets sent messages
function get_sent_message($username){

	$sql = "SELECT *
			FROM `messages`
			INNER JOIN `users`
			ON `messages`.`sender_id` = `users`.`id`
			WHERE `users`.`username` = '{$username}'
			ORDER BY `messages`.`sent_on` DESC";

	$messages = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($messages)){
		$rows[] = array(
			'message' => $row['message'],
			'sender' => get_name($row['sender_id'])
		);
	}

	return $rows;
}


//get messages by id
function get_message_by_id($message_id){
	$message_id = (int)$message_id;

	$sql = "SELECT
			`id` AS `message_id`,
			`sender_id` AS `sender_id`,
			`receiver_id` AS `receiver_id`,
			`message` AS `message`
			FROM `messages`
			WHERE `id`='$message_id'";

	$message = mysql_fetch_assoc(mysql_query($sql),0);

	return $message;
}



//gets supervisor or second marker
function get_supervisor_or_second_marker(){

	$sql = "SELECT `users`.`id`, `users`.`name`
			FROM `users`
			WHERE `users`.`priority` = 2 ";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'id'	=> $row['id'],
			'name'	=> $row['name']
		);
	}

	return $rows;

}


//new meeting
function new_meeting($assignor_id, $assignee_id, $start_date, $end_date, $details, $location){

	$sql = "INSERT INTO `meetings` (`assignor_id`,`assignee_id`,`start_date`,`end_date`,`details`,`location`,`date_created`) VALUES ('" . $assignor_id . "', '" . $assignee_id . "', '" . $start_date . "', '" . $end_date . "', '" . $details . "','" . $location . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$assignor_name = get_name($assignor_id);
	$assignee_name = get_name($assignee_id);

	$log = "You scheduled a meeting with {$assignee_name}";

	$message= "{$assignor_name} scheduled a meeting with you";

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message . "', '" . $assignee_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $assignor_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
}

//get meeting by id
function get_meeting_by_id($meeting_id){
	$meeting_id = (int)$meeting_id;

	$sql = "SELECT
			`id` AS `meeting_id`,
			`assignee_id` AS `assignee_id`,
			`assignor_id` AS `assignor_id`,
			`status` AS `status`,
			`remarks` AS `remarks`
			FROM `meetings`
			WHERE `id`='$meeting_id'";

	$meeting = mysql_fetch_assoc(mysql_query($sql),0);

	return $meeting;
}

//update meeting
function update_meeting($meeting_id, $assignor_id, $assignee_id, $status, $remarks){

	mysql_query("UPDATE `meetings`
				SET `status` = '{$status}',
					`remarks` = '{$remarks}'
				WHERE `id` = '{$meeting_id}'");

	$assignor_name = get_name($assignor_id);
	$assignee_name = get_name($assignee_id);

	$log = "You updated the meeting request with {$assignor_name}.";

	$message= "{$assignee_name} updated the meeting request with you.";

	$sql = "INSERT INTO `notifications` (`message`, `user_id`, `date`) VALUES ('" . $message . "', '" . $assignor_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $assignee_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
}


//gets new meetings
function get_new_meetings($username){

	$sql = "SELECT `meetings`.`id`, `meetings`.`assignor_id`, `meetings`.`assignee_id`,`meetings`.`location`, `meetings`.`details`, `meetings`.`start_date`, `meetings`.`end_date`, `meetings`.`status`, `meetings`.`remarks`
			FROM `meetings`
			INNER JOIN `users`
			ON `meetings`.`assignee_id` = `users`.`id`
			WHERE `users`.`username` = '{$username}'
			ORDER BY `meetings`.`date_created` DESC";

	$meetings = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($meetings)){
		$rows[] = array(
			'meeting_id' => $row['id'],
			'assignor_name'	=> get_name($row['assignor_id']),
			'assignee_id'=> $row['assignee_id'],
			'location' => $row['location'],
			'details'	=> $row['details'],
			'start_date'	=> $row['start_date'],
			'end_date'	=> $row['end_date'],
			'status'	=> $row['status'],
			'remarks'	=> $row['remarks']
		);
	}

	return $rows;
}


//gets new meetings per user_id
function get_sent_meetings($username){

	$sql = "SELECT `meetings`.`id`, `meetings`.`assignor_id`, `meetings`.`assignee_id`, `meetings`.`location`, `meetings`.`details`, `meetings`.`start_date`, `meetings`.`end_date`, `meetings`.`status`, `meetings`.`remarks`
			FROM `meetings`
			INNER JOIN `users`
			ON `meetings`.`assignor_id` = `users`.`id`
			WHERE `users`.`username` = '{$username}'
			ORDER BY `meetings`.`date_created` DESC";

	$meetings = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($meetings)){
		$rows[] = array(
			'meeting_id' => $row['id'],
			'assignor_id'	=> $row['assignor_id'],
			'assignee_name'	=> get_name($row['assignee_id']),
			'location' => $row['location'],
			'details'	=> $row['details'],
			'start_date'	=> $row['start_date'],
			'end_date'	=> $row['end_date'],
			'status'	=> $row['status'],
			'remarks'	=> $row['remarks']
		);
	}

	return $rows;
}


//get document
function get_document($id){

	$sql = "SELECT `mime`, `name`, `size`, `data`
            FROM `document`
            WHERE `id` = {$id}";

    $document = mysql_query($sql);

    $rows= array();

	while ($row = mysql_fetch_assoc($document)){
		$rows[] = array(
			'mime'	=> $row['mime'],
			'name'	=> $row['name'],
			'size'	=> $row['size'],
			'data'	=> $row['data']
		);
	}

	return $rows;

}


function get_document_name($id){
	$sql = mysql_result(mysql_query("SELECT `name`
									FROM `document`
									WHERE `id` = '{$id}'"),0);
	return $sql;
}

//comment document
function document_comment($id, $comment){
	$sql = "INSERT INTO `document_comments` (`document_id`,`comment`) VALUES ('" . $id . "', '" . $comment . "')";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$document_name = get_document_name($id);

	$log = "You commented on your document named, {$document_name}";

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

}

//delete comment document
function document_comment_delete($id){
	$sql = "DELETE FROM `document_comments` WHERE `id` = '{$id}'";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	$document_name = get_document_name($id);

	$log = "You deleted a comment from your document named, {$document_name}";

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
}

//get document comment
function get_document_comment($id){

	$sql = "SELECT `comment`
            FROM `document_comments`
            WHERE `document_id` = {$id}";

    $comments = mysql_query($sql);

    $rows= array();

	while ($row = mysql_fetch_assoc($comments)){
		$rows[] = array(
			'comment'	=> $row['comment'],
		);
	}

	return $rows;
}

//get notification
function get_notifications($username){
	$user_id = get_user_id($username);

	$sql = "SELECT *
            FROM `notifications`
            WHERE `user_id` = {$user_id}
            ORDER BY `date` DESC";

    $message = mysql_query($sql);

    $rows= array();

	while ($row = mysql_fetch_assoc($message)){
		$rows[] = array(
			'message'	=> $row['message'],
			'date'  => $row['date']
		);
	}

	return $rows;

}

//get activity_log
function get_activity_log($username){
	$user_id = get_user_id($username);

	$sql = "SELECT *
            FROM `activity_log`
            WHERE `user_id` = {$user_id}
            ORDER BY `date` DESC";

    $log = mysql_query($sql);

    $rows= array();

	while ($row = mysql_fetch_assoc($log)){
		$rows[] = array(
			'log'	=> $row['log'],
			'date'  => $row['date']
		);
	}

	return $rows;

}


//checks for student without allocation
function allocation_exists_count(){
	$sql = mysql_result(mysql_query("SELECT COUNT(`allocation`.`id`) AS `allocation`
									FROM `allocation`
									INNER JOIN `users`
									ON `users`.`id` = `allocation`.`student_id`
									WHERE `allocation`.`supervisor_id` > 0
									AND `allocation`.`second_marker_id` > 0 "),0);

	return $sql;
}

//get all students count
function get_students_count(){
	$sql = mysql_result(mysql_query("SELECT COUNT(`id`) AS `students`
								   FROM `users`
								   WHERE `users`.`priority` = 3"),0);

	return $sql;
}


//checks for student without interaction
function interaction_exists_count(){
	$sql = mysql_result(mysql_query("SELECT COUNT(`dashboard`.`id`) AS `interaction`
									FROM `dashboard`
									INNER JOIN `users`
									ON `users`.`id` = `dashboard`.`student_id`"),0);

	return $sql;
}


//get total messages
function total_messages(){
	$sql = mysql_result(mysql_query("SELECT COUNT(`messages`.`id`) AS `messages`
									FROM `messages`"),0);

	return $sql;
}

//get messages from the last 7 days
function get_messages_seven_days(){
	$sql = mysql_result(mysql_query("SELECT COUNT(`messages`.`id`) AS `messages`
									FROM `messages`
									WHERE DATEDIFF(NOW(), sent_on) <= 7"),0);

	return $sql;
}


//get all supervisors
function get_supervisors(){

	$sql = "SELECT DISTINCT(`users`.`id`) AS `supervisor_id`, `users`.`name` AS `supervisor_name`
			FROM `users`
			INNER JOIN `allocation`
			ON `users`.`id` = `allocation`.`supervisor_id`";

    $comments = mysql_query($sql);

    $rows= array();

	while ($row = mysql_fetch_assoc($comments)){
		$rows[] = array(
			'supervisor_id' => $row['supervisor_id'],
			'supervisor_name' => $row['supervisor_name']
		);
	}

	return $rows;
}


//average messages for each supervisor
function average_messages_supervisor($supervisor_id){
	$sql = mysql_result(mysql_query("SELECT avg(messages)
									FROM(
										SELECT COUNT(`messages`.`id`) AS `messages`
										FROM `messages`
										WHERE `messages`.`receiver_id` = '{$supervisor_id}') as counts"),0);

	return $sql;
}


// add interest
function add_interests($name, $username){
	$name = mysql_real_escape_string($name);

	$sql = "INSERT INTO `interests` (`name`) VALUES ('" . $name . "')";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

	//log
	$user_id = get_user_id($username);
	$log = "You added a new interest, {$name}";

	$sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $user_id . "', NOW())";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());

}

// retrieves all interest
function get_interests(){

	$sql = "SELECT *
		   FROM `interests`";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'id' => $row['id'],
			'name'	=> $row['name']
		);
	}

	return $rows;
}


//delete interest
function delete_interest($id){
	$sql = "DELETE FROM `interests` WHERE `id` = '{$id}'";
	mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
}

function get_interest_name($interest_id){
	$sql = mysql_result(mysql_query("SELECT `name`
									FROM `interests`
									WHERE `id`='{$interest_id}'"),0);

	return $sql;
}


// retrieves all interest
function get_interests_lecturers(){

	$sql = "SELECT `users`.`name`, `users_interests`.`interest_id`
		   FROM `users_interests`
		   INNER JOIN `users`
		   ON `users_interests`.`user_id` = `users`.`id`";

	$roles = mysql_query($sql);

	$rows = array();

	while ($row = mysql_fetch_assoc($roles)){
		$rows[] = array(
			'name' => $row['name'],
			'interest'	=> get_interest_name($row['interest_id'])
		);
	}

	return $rows;
}


//retrieves project title for each student
function get_project_student($student_id){
	$sql = mysql_result(mysql_query("SELECT `project_title`
									FROM `student_project`
									WHERE `student_id`='{$student_id}'"),0);

	return $sql;
}

?>



