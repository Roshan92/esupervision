CREATE DATABASE  IF NOT EXISTS `esupervision` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `esupervision`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: esupervision
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allocation`
--

DROP TABLE IF EXISTS `allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `second_marker_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `student_id_UNIQUE` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allocation`
--

LOCK TABLES `allocation` WRITE;
/*!40000 ALTER TABLE `allocation` DISABLE KEYS */;
INSERT INTO `allocation` VALUES (9,6,9,8),(10,7,9,8);
/*!40000 ALTER TABLE `allocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supervisor` text,
  `second_marker` text,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `student_id_UNIQUE` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard`
--

LOCK TABLES `dashboard` WRITE;
/*!40000 ALTER TABLE `dashboard` DISABLE KEYS */;
INSERT INTO `dashboard` VALUES (2,'sdsdasdas','wqeqwewq',6),(5,'hey..supervisor ..u are funnier..lol.','u are the best 2nd marker',7);
/*!40000 ALTER TABLE `dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT 'Untitled.txt',
  `mime` varchar(50) NOT NULL DEFAULT 'text/plain',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `data` mediumblob NOT NULL,
  `created` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (1,'COMP1661_LOG_T2_1415.docx','application/vnd.openxmlformats-officedocument.word',29222,'PK\0\0\0\0\0!\0aDե\0\0\0\0\0[Content_Types].xml �(�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���n�0E����D���(,g����@]tM�#���\0g��������&��ȹ�pH]�-֐�_�a5x���Z��}+��Iy���;@q?��a4�E���=�bI�J�z	Na\"xiBr��5-dT��Z��<�I<���:\r1=B�V-�->�$hQ���W-T��Պ�T���ǥ<:T\\���K�cy֡�lp����I�@1U��+�r��&��5T/˜�Mc5���ZLA\"�ܵU?��\'��~�搸��Az�,Ү|�����--\'M�O\\~S�]竃ųڼq��1��?(s;�G�,��?oF�L<҄@>�-����B�77b8)g\ZΩ���p��{�襳��r���c/�%��4����?�}J뮺�����B����w�����1`�x���6~\0\0��\0PK\0\0\0\0\0!\0�\Z��\0\0\0N\0\0\0_rels/.rels �(�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���JA���a�}7�\n\"���H�w\"����w̤ھ�� �P�^����O֛���;�<�aYՠ؛`G�kxm��PY�[��g\rGΰino�/<���<�1��ⳆA$>\"f3��\\�ȾT��I	S����������W����Y\rig�@��X6_�]7\Z~\nf��ˉ�ao�.b*lI�r�j)�,\Zl0�%��b�\n6�i���D�_���,	�	���|u�Z^t٢yǯ;!Y,}{�C��/h>\0\0��\0PK\0\0\0\0\0!\0�_��\0\0�\0\0\0word/_rels/document.xml.rels �(�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��MO1��&��M�lY�6.\\�ă�x.��lc�i�A��;JX@`I�^�t�����vz7�[�|B�\Z].��/p��Ms�6~�]�$�r�2� �b4<=�{��7�Z������������������+���a*�*>��߿�aSC�4��2������ǵ��t�X�,8ړB֬�v,����U�k���0�VS\n�L�� �0�v�z^�Қ�Yi<c�xs���rg갺���@+�%��_m�##-�wM�CUAAq�>;K)�~���\0Ǟ�:�\'��!�h�\\E�]\",�_,�m�.ӻ��@�ײ&hBm�]BT����PD֩ď�6�N�A\\t��L^���c�Nn�@2�}ݵ�\n���l8҄�(λ��;^�\"m7]\"���Z4?�H}��ʭ�h�\r\0\0��\0PK\0\0\0\0\0!\0cuZ��*\0\0]D\0\0\0\0word/document.xml�]�n�Hv� �@�40��|�vb/l��qڞ$�Y���H.IY����ȿ<K%O��T�(�DZ�DJ�\\�Ũ-^T�S���s���?���ȣ�\r��Nwo�cq�׿?��~���ێ\'�w��������_/����u|��h����+��l��A��go����Y�7t�(��~�g�7A�����8��7��}�0\nl���k�?����n8�� �>~�DC��{At�fȢ�Q�\ZoY��\\�M�������3���t@���#gr@�z\"��E���\'ߧ+ ~�M�=�!��N����0Ł\Z��s�xz�q�=���lʋ�������μ�`1��Г�@�;���7v���L�#�l�A�M5�!s��5�-M~qq\"V��OQ0\n���jo��?dYad�\'���Wz��ѽ��w��}���\"��0�q��\"��\\�Y��>���~�D�?���g�����>��x\n�ph\'�7�\rW�p(�W�G�w�Q󄞈��:�\n����{��p��(	�<�O�~�$I0\\��Ƚ,��oĪ���]s����\n�%������I�]ΦUv�\Zx�u9��`z�� xP�t)�w�8��%]�&��M.^�hHH]W_�[���dP��~�7�W��(ǐQӧ�u�R��w���G�\'\'r=����-���+��oV/L\"�r���a�\\]�����n@i�_����O.A؂xm��t�vJ�����i·�[^F�t8\0ˑ��g3�(8q�V���ô���\\�������/��#���>y��^�侢M\n���B�gX?ᴟ�\\\"y9��I��}��s�yl�����`� }�g6��e�2O>ۻ�c�	������?շG�k�q���cb\\�;����nŲ���3�A��f)��Xy��s{���BUͥ�!c�I�S�F<��#�\\\\�(�g�8�r�b��Ml͛��^\\����{r��&E�&�OM-G��\"F�Y�>���\'�\r��ez�-{�=�^\n�V��-����k�5q�^\n6F[�����;���?Q�Ě�Dߖ���3RG\\�a���	>\rC���u4����@��)NVs;=8�~�Q�We���հ�oY\r�,���\"��*�Ыi����@�.�3��4	���S�p�U+�x͉إ��\n7)覬�n���錈�WÙh�S��h��I��F��m�NyF�aG࣋RRp͆�i�&����O�Em�|�H�栱X��H�_&�ԡ�\'���:ύE��\"OԌV[;�%��)������dp}�љ�+e��h�H/p�SW�6��q\rܿ|W\'���z��5߂�c}fq�|V�O�`���+��>k;l��X�궦��5��Ŀ������`9�a�SL�9���M�z�=z�=^�	��&��\0`��V�9/���aI�9s<ן��k]�\Z�=u��_�X�R��%������0n]<���B�\\�]]�3;R\"V�`\'T���h}�y���ݻ��bdw+y9��\"D�ې���E`���Ezb.�(�\Z��cNX���,XT��l�h�Y;$��^hF�s~�Ʃ�M���A0�+a��Ab��{��!��xXx\0��$x���[c7X	Pz���;X��X�=Y�}���:!\n����&,\"h��������\\1�A������dwJ.1�`�ɜˁ~�g�:Xs�}n-��jv��,�	1��(�:�9���%O ��.��~�����Uf�mHLs�ȆaJ�G�7#���sp$\Z�������fZ��v>��9��Y���ҏ���2Ti�\'�F��U�����O�b���P�\r�\\a�\n����\\Lr�ʋ\\�&t��Ȳ<a����)dC���nC�P��pt<YЂ���F]���W�H�uɿ�?�����@��񮚔0kH�-����+���\\���KϽ�\nyuˁ��J��d�f&ii�j���V�p�s!���c�.��x�i��)�\rO��3_dq�	1�J�������FBA�Yl=������g}I,׷=x\n�3��\'z��e�/�U��F�N��GH�K����, �����J��iE��#�1��t9Ȼ�/V<�\r�D������a�����:#�\"x���K6�x[�|b����#E�I\Z��O.�!�* �Z,�#N~@n�_\r�<{��?��(1i��Ìn0Sϳz�}�q��c��x�۷�{�X8_���GQ��������.̶����i���7r+-�b��ĥ�}W��\'O{3ĆNe�d���La���t��+<����������3�	�d���������|�<�E�W�p\'t<#�����w�P}vN��y��,qkDU	��ģ׹�k!��O�D뿓,�\\����퀎�V\Z�k�)�U�������%��%	�	q��S �E�ͱ_�\'xI��	\\H�G2�$�����e��\Z��߇\\�:Q�~���-��n�-y��>R�u>Z6�o��H*%,gϧ9��,���4q��n��+���\Z�ǖ2Q�vA��ꙡݓ̯����j�:���׵��[u�������9�`A0|	�R�ߨ�S�TD���?o0�+0GJ�\'N}\r�\Zs��oI)G	 ����#���+�x��N�?\ZJ[��&�-\r���\n \nn�_b\\��qKp�K3??���L��*xI�?	����\'Lnډ����]�_E`��\r�ϺFJTu��E<���\",LAj�$�k�����rz#e���u|���l��	�;��@�dV*��\"�H���ag�g�U�*5\'��$�;P1IU�M���&\Z%�!�A2`p�İ���rK=��`�>um�Z���8qǏ܏�]a.KC��XB��/Q����y�� Y�.:ߺ�>0=�/�7�5���N�Cu�|�ed��KmH���i�����h�����y�C:O;���pG��S�7W�H����|�;���q,�^��6d$=��@�#)�5�i��#�MB��/���Z�b����Y�[r3ϛ�}Ć-;ۗ����Y�s�L���L��J�����GF����biqMU`}���	���!�TM5�L���/7V�*i٨����C#c\Z�7�\'�k;���e�\Z�Ҍ�8/J	����\\����*�������ڊ�!_��U��Q*D���4��j\"2��������}�$z5��P7(�$}Q̢P�X\"\"J��QH��~=vcz?��\r&�(~-�4^��0��bT/���Ga�w^9��\'{d�G��\'؜��?$@�c��ZC�~�\"�h���d�tHL^�:H`�QO��uߧ\n ?/�	 �Q��v�����\nQ�>�.\nLt:k�U\Z7^��W,�6�<?�*OZ�!�\\+]�Z0k�H̢J\0;�x����2_@�}�B\0k�>�jZ(>�m�B��h+��Ԉ�����(���!>�x Vu�Ȟ�*�@�j%y�|���/BoT!�N����?�P/��k7B܌�g�<�e{�E�t�D�SC����:�Q��)��++�p.o���ZH�9��B��B��#�W��-aO�@�P��ǁ5��~�zC�Q�s�l^��E�,����5`�?a�>�H9��wX��6`~y(�!Ã��H���� @���ͻ���!���I������O�[p����?y�F�O2�imZf3��c���݌W΅��oE$��+�����@������A�q���lԷ�,�P�C�7Xv~�\\F�����:�98���\\q�i�����Tƴ������ܿXH����Yd�A���\"�q�L<_���S7QpO�B�HR��%M��\n�D,��-X���D?`v����:G>�\Z����N�@x0\0��O½���!eG�]$=äN��Ea@T��.r����D�1U5�f{FA�Oў��jIi��,��$z+u�Jʩ*h��!א���%���<�Dǉd�\"��W�ì�Y9�q��Ɠ����x��iVn�+a�b�М�9���%oF�24gh��\\�iq�;�Z��1���NQ���:t��L\0����`7{E�j�D�))�Ԕ=��-�K�o>\\��?8���[(���.{�e/���DK{>XL�++��?)]3�%�?�6{N���4ע_8�]��pCA�g�~`����\"6D9�)I�R��D�2g��)�˼C��.��t����]�p۳\'����B�#RxLhe��I;Fb�!k�{Ij�{8>�t�[Nr��\"�7k*��Nlߗ���j�n\r��1:]a:2�P�ED#���c����G6\0��/�9�f7�ߞ��G�\Z���o���j+S9KSIU�bYr^Z@�we�X�=˺D�,舀�ʈp5����]�����7�\Z���)��\Z5r3]5������H�\\�i1�6���J��C���9{`�力l�vI�h5�����H�G-4������$��kܢ �u����O�ʴ@b�9��M�,�Kd�aJ$t�xE��F��eJM�E��+���e����~r�9DQ2a��U�oEAǴbj2�&b�&R�CʷoAˤy6����F�B��`�`Z�[V[��s�զ�(?�~џ)���6:x5I��Vh�n�z-|���Z�h�}q�\n_�C�=��N�;�:B��H��$<{����H3	�=`���3z���X��0��MH�)�״LݽA2��ޒ-��^���Rݱp\rԗ9.�߾\r\\�1F܄�\n5R�\nB�ahq�p �R�z1�>��\ZFb��6��Ec�c��S�h�B�\n��}{%�K�:�/Ŋ5#��\ZU�x	��~�yh���e�*,i���5	�\Z�A�S�,JĈ�F�5����^pO%���]Tk�����+%���h���;��Y��h5 j��H�G-�A@��\n�8���~\\�K�Ȣ�4=�&\'�!e��k�T�yt�;�%��^¨_0;*\\c������f�����FJ�J�r*?SQ�\\|�����P��|�l��!��$5*R�ٞ��{�#��Dd�SaMu\"�&$b��@v݀h}��\n���O/:#��S��K�H�~5h��)Xޟ��B��Y�t).&�X(:�eL��p�,�ZD�q���9(d(�FGiS9\rd���ǴHEJ��)��D�W��~�yD��	JO{!(�\\�����Z��s��*r.�H	N������\0���R�Ӛ{n��_ܹէ_�j�T�١s��k��^B�Y�o^n�	�� �&�}OH�&����5��M�P�咋o�ǿ�x���e�!(��M+UV@�^�ͬ�(����B���@ac.�mb-M0\rW�a�$�Qn��Z�z��k=sYm�%�޼cu��߻�\n�\r!q��8�����lUKPvii�e�Kw���t��K4�ݐ���n�ʍ�[Z��<*�N�A�\\����|d�l�<����)�_��P�\nd���2�h\r�Q]3�@��࿁�A՗�E<+�q��ӽ�MW4\\�/�p�l{�gV6��݆)�]Kʐ��\0����/8�������miK����;N�{��Fܾs�;Q�>���C�2����6E�U�5;�5V�u��W��X��؎\\ԗ#m!※�]<p�2������\rnngh�\r�px�C����y��Oh\Z�!�K���aR�TԫMsf6�1���*�P�M�3 T���L�]Q�ʹ���,�\n7`��U��z̈́\Z��e�\"T�8?s�c��DU+���Eqf\\$	�Pte�f��<ۣ�]�\nS�*�>-�/+�DmYauFc�q�c�j���K�yƄ^U��K�y��VkTz�;��R����O�ݘ���QlQ|�(	U�\"L[�U]hi�R���8�_�Ϳ�8Hp�,ڝ����0G�i���^��MU�_���Aw�j|��14~DwR��H�&���<�ʻ{l����Bb\"yH���Rڌ�e�t�<��#P�)],�#�\"���+��1���2v��J�YS`�hY뱞cR[�e׌�k���]���)��\" \"༳���s�Zr��l,y�!1���gS%�â�J�\"��\']X�����\Z,b�����d�PM=Z�|�&�[a:�����@�q�.\nQ����O,�9ɸ	�Ē�\\Y;��eЎ[��|3���=���E�\"��Hn�/e�)�����/�7��E:,�]J�cH�C��Y��1�W��h}�l?i����ݣ�F%^��.R�����qN����!�]�\Z;I3gb-@��4-. J�����e��;$Xv�A[�ej�\'����&ĦL�\r���t�r��t<pgj�4�f�����5W\Z���Ԟ�^�ި\r�19ɛ	Q�\'�RR�^pI?�=���埯��~�NJ*Ǡ˟���=	S,���<u�L�A���� ��I1i�;\n)P��縳�7���-�{��A����a���m��H]�D�(E���+�����09���zݔ�*t3��\rc��;��/�c�����W)W��L	W��l�~{��������ĆWn�zfo��+�f��`��?����E�6�#\"Q�x/F�oݭf�]���d&���������8�\0Y�K5t3E����&��.NEע�&zT����d�����l4�\rk\":��M�e�\\��\Z.\rs5�k頸MlG���5���&���s��$�OEy�^�	�t���|�Cυg��<�¨D]��T��]��O\Zy)�[^�Zk�٦�`5�Bۙ㽢N���R��bU<�})a�{�Ɗ0�i.�Qk��w.\0f��f�Z(BӤ�i.9��\0�4J�K\"��:_���^����WH�5�5�}�U\rv�N�����h�d�[�~���\"e�$���S�D$��������݀��u/�_Ұ��U��M��������Z=�ׂ�`�9J�J\r���#\"U̒�v�~���8q}�!����?	���Z��;S�l��֯�˴�RD_��8���j����D���\'��jC��M�\'�U��b]��u���ھ)���)䝲��!K�}5�Ƹ�Fbj�[��r�K��uR[��c����t�\'�o�~�Y��Ƿ�\Zn����Uf���ळ�Guz/>�}X�<����H��9y����$��6��I�8a�$֐E*���$�N�������ubh��dX�9�B	@X��W��uvh�g�&���\Z�l��2*�4�<��<�Q�Y��g��\\>y7@1������w\Zү�F�Ow���o��ՙ��k:��!�zQ��i�w�?S%��%��8F-A*�e�R���Գ<�ȅ\Z�8i8�Ξ6��^�!�vCs�w����R�0:����\\@���p�l�l`\0�PP�Ls����κ钷���ס=u\"�ug��X��������W��GTJ��\r��w��D�/��5jX�-F����T8ŶGT�3ʩ�ydBĨ�I���*�(p�X���\rd�GzV��<>%����n����y\'s���r0��X��dk��O�VD�Ǩnk��k�ir����:��F��	|=����ӵ]�GvV�Fh�w���p��2!��vDɮ4�N��g���ȗ��b8�A/�\0��p(\"y(Z�_U�m�N�E���r�&����w�\Z�F���3 +ug2n�b��ܰ$%N�4���\'��e	7h�B��9��Z)^�p�� ���L����zj)O�ρ�b��\Z�~��<���yn/r�e*�]N���?��;{d�y�@|�Į�z=!�F�Z���v�=9�Z_��^<X�yD�u�\\m=�.���E������6yBzdJ�_�7r�\"���� �Ґ�T�q|�c]�����h�z�#1�V>T��ee�q�.n��C�Aj�!��\\�h+	l��j�\n��}P�F����g�+(H�A����ްH��RAm��u�zV�Q^^:�F��}��B&�GC�)�Jm�f*r�����\\���Ÿ�a����B����x�S+���q�{�&k3�i�2��}\Z���-#���ni�w�Z[e�\r�o&	���]r��!#�@!�Kz�>�J�=�XqcB��S6ߘD�p�U\0N���PلҷG��\"�.��N�N�P\\�x��P�n������.�t��K��Ĉ�?/����-�8x�������3I�q��rt�`��\r\"BAM.^�h�箫/�-~��\nQ��?���Ka���)r\"�{|��5������\nh_�v�k9\r�dᡬ7��Z;��}w�����{y�Dh����k��+F�g$;?X�8�Rx���V(O����لTi��|7{���s������2�t��-C����SZ[�����>l�M�u4ڊ��gD�03���*�.J����_-�ε��	Z�EA�7�,�r�B���gN�8� �6���ާ��z�ğ��O=���%��7M\0��r�s�=�c��@�5:3��m��Y{�x)Q��A��h�⋕DT��˩؞����#�Pgi��Jʖ���>���\Z��Gd���s��#�88`(LA0>���&3$&B7��Uv�E��H��)Ȗe\n9J:CT\rUO����5D�hz\0\'<�fPV���G$��L蓩�[�5=\0\n4ƦJ����MڂŶ��\r��=U��.��$��;��d���N_ۢg*��8Np����G�!D\"�1?Ss���ǈ�N&%��u� I�ݴ�\Z1�?r��ڊ�dl�=0f�[�\0����5����Ʃ �;<I�/�4�3�z1s���{�<�Y�l�>T�y3�\\�x^��K+����Y����^��Ɲ=�ƅ�I#�-}�\n��?F[:�K���~W;�B�?=����у!���*TN�8G@V?B����\\y�����g�-@_�z�f�\r�oϮ��ĥ�l�n�Wk�����d�:S�i�`�>�Ё�K]Po���*��A�CsZ`���#�p��X�-�\0�o�/���⁦\0�5GA���[P�	b�VPq���s0e�DY[J#�f��Tv�J%�vzO\Z�^j�Q��JYF(U@Ơ�%,7A�H�RV��Ŭ�0(ehU���!$x�j$T3H\rJ9s�@CHRD��*KhF�p��L��įkP�i�\nzk��iPʹD�}�E�g���TeP�&�\'��P�\rk\"1�R��Λ�[ա�:H\Z\'-�R��\n�X��\\>th2?k��7�\n��]�R!�f���뼢�y�R��B���C:�~\Z�y��\n�^kt�	#w�����1�,teׅQ�*ne�lω�8V�ԋ0�k=1re_E0k�\rgO/sF�Ν���I���	���q�4ܖa�Ԟl��l�APÚ�sM~2-�]�e�.\\q}-\n.#��^����/�K߉Q���by�Qx����5[����l���\0�o�����Q��|H=3�y�)s����i��,ND�^S��������!�8S�w��#o*�ƪf���K�Î֑�aY��z�A^j�A�\Z�+uE,hG7!�9^|CUuT:���M}^SX�5�A�B��A�\Z��t��z9�e����:�\ZlK�[�ښ����VN��聩�{ut}��#0qi���*X����A�\Z�+�%�z Y²�&;��kwwV��\Z�kQ�ʥ���\ZK�V\'^�䜻\\�\r�W1��������M5ii:B���eW�yUS���9ߥ�X���%膃�֥���n�D��������MX�P4�zo�a\'��W6��$zZh�L�T�,���È�<z�Q�W[��֤ZF�J����Tp�.o�Xi�.z�9�0q��\r\\�C�c4����n\'�.�)�\Zњ��b�<���(	H�DJl<i��@ޣb���Vԧ��}��\n���ќ����gt�s��ɟ�Ѥ�=YN��l7���;�~�Xl��S�,�z��U	J2\"-�j\n��88��\n4���\0���u�E\0�y����0�x�-7�N��.\n.�>�[�f1pQ%Ů9!}a�g�ߣ��)�Zs]IZ.�g!qL�T�\'���h��(TQS(խ(c�wj�M�7p�\Z�ʪ��:,�C���	H���i�t?�%�\Z$�˪2cˡO���Vȩ��/�� 3EU\Z(Q��cO�S(u�iĳ���L��̅}f�Pj�e�Υ)�j\n�R�C�0�88�\\]yCD�&�A�$�{!�ᖧ�8ܢ�Ԁ9�G.]	&��|�jK��k��U���R%�f����f�R���Z��k�����k���]iIB��[���.�fe�e�P�1��#���- y.���^{����[Ӌ��G�;�VȞt��!��Nz�Q��H�<ܬ�v?-���Ӵ\Zؓ��ZF>dع�f�ES����I2�s~�q~�Կb��&f*��(SŁ#傁��*�g��\'39\r�U��+�U����7\n߬�a�H�ǩ�(U9�N�&\"����ܳ�X��\Z\'y�����$�і��Қ|������*�vn���/dg�,��T)�pP��4�Wx��$���T�t����!H���a��[�^Lx+�;=X�a`�ۋ\\��\\�YG��h�2���Z۷�f�����3_$�R��sIx�X��D@\'}���;4����Wj\rg�f2�s�n����� Ɨ�mM�f��Q�D� ����2d���<����\'Z����h1²,�����WJ<s�&�xRQG��Hk�n*ֲ�{RAE[��~���`��S4�kS�ռ�Sqﬔ��˔���0����0�kj�n�s؀����=�����\\���Kq�V��JgS�۔�&5�����̵�\\O�j�^�Է)����4�{sm0צ�w!cF\0,��_[8�`�;����~t���\\���\\øj`d̵�\\̵/��V`�U�\0\n�POĽ�A30�����HF̻\rk,�-թ-�P�ZS�\0�������5y�jɢ�~CeUs�bkÛ���|۫�,T���FhM>Ԍr�d�;Ҙ��M���[)�U�po��ztr|��T*���T�p�w��{�\0�>y{�V�]��o�~\'	B|x\"��{?��������� I���������w�>�� @ѓ����@ɡk�oL���p�S�zpq�>�q�<<�\\l�\\\rQ\'�8O�xd4�~r��\0\0\0��\0PK\0\0\0\0\0!\0ȹ+3�\0\0F\0\0\0\0\0word/endnotes.xml�U�n�0}�0���N�t3�ۂ\r}+z�\0U�c��(Hr��})���A���^l��!Ez}�W�hϭ�s��q͠z����߳o$r��J�<\'�����u�q]h��E�]����{�űcW�͕`�~�@�P���[ċ$M�/c�q�0�/��ԑNM��p��J��z7���O��!��^<\n)����\09���zB��Pp�:B�k�\'�v�[`��ڷc�%r\0�*a�2>����������ùƤ�I�Q�[j����R\'p\'�QtNJvy�=V�_�49\'��H�9����E�a>����Ŏ����c�6##>�v��F�И�`����{)ͽ`Һw5�D�e�;\r�>JdԤ�(�H�9�������Z��4�\"\'��=gp�è��	v�ϋ���p�5myIk�;7��X]�Xu�̍\r1��/3���s�knM&EH�b9.n����H�Y�Mֻw�nm�@����)u��n��݀0(M�K�\')��i~\0�g\0\0\0��\0PK\0\0\0\0\0!\0#����\0\0L\0\0\0\0\0word/footnotes.xml�T�n�0}�0���N�l3�݂\r}+��TY��Z� �����l��� ��/�P��9����YG{n�\0��t���+�P���~�9�J\"�*h\r����-��|��n��)p�F��l�j���9�űe��Υ`,�n�@�P���Sċ$M�/m�qk1���ԒNN�@s��J0�:;��%5ύ�!��N<�Z�b\'�\09i��B����zB�+D���3y��-�Fr庌��5r\0e+��2ފ��@iI�^�����r�o���l\rm�G�	ܙb}���:����?b�\\3t�C�^B�ߜ���B�0o+�iqq$�s�h�HG���ݪ��O�+�%�n�N��WLF�����H��v��Ч\Z��2�7�lN�E�f���rM\ru`�D��Y�9j��uT��������7�љ���M��\'w޴X]ݬ�l����VS���i�86��6����b9��7�.m�x���l�1��mޡ{�\rxV�j���0���CJ=K��l�D����\0\0��\0PK\0\0\0\0\0!\0��\r!�\0\0X\Z\0\0\0\0\0word/theme/theme1.xml�Y�o�6��� ����$�A��fk��ۡGڦ-6�hHtR�(0���u�+��ö-�K��d�u���=R�L���9C��E}���{��H�굇!��p�5��m�h��$�6���^�n[	G�Qᦽ��}m�����[`%;�i��v��d�(��f8�w���c<-�ct~CZ��J^1D$������dBF�\Z����y��c��0�q_�ƚ�Ď��,����M���~�m���Ë�]�vq�j�dF�o�U�z�/��Ƈ�g<�:��x�ܿP���ֺ^���I\0\Z�`�)է�n�;n�U@�O��N�S-kx�u�s��\Z^�R�����!�\Z^�R���w�Z�w4��xo_+�:NM�KP@It��.�^�_�6�L�3��ӫU2�+̆|v�.&,���Z���\0��������bQ2���O�ݠ���i�(�h=Z�(&3޴?�!X+������/��\'�N��r���ɓ�SG�������������_/�{��+3>Q����o�~i�\"Z�y���?^�x������\0o�h��$ĉu[wX�Qљ�a�n�\0բM!ы��\Z��Qd����{1��	x}�@#��9\'�7�P0F�,6F��K	�`M͝�sw�#S�>���v�3PObr�X�y����)�0��;v��at�	��z@F1K؄[���F��j�ie�GB���D������f�4�>ґ�*5�`���:�s�\\PHՀ�#�H��H�u��bʬ�\'���V�U�~Ĝ��ud�ɡ��>bLEvء�pf��I�؏�C��Ⱥ͸	~��\"�!(ښ�{k�>]\r�v��VD��ǆ\\^�L���� ,��]S�D��w���	7H�o�x_V�n�ĸf�քzn]�}���W��G�1,���^�ߋ����m���%y�� �b3�n���;ܺ��J�|A�~\"��	Ԟq\Z��<w��,6�X�Ё���H�X1��4��{�N�I�z�X3���Q6}<��l�9�eq�L�#A|�^r�v8.���VǨܽd;���%a�.$��tU�ڲQI�!hrd¢a`Q�`����Ȃ-U�v0#83!��\"Oi��ٕɼ�Lo�6J�]#��L7׭��K��2��P��NBFFְ$@c��N�z\Z���*�\Z=�,\n�Z��X�7�`��\r4R��F�q���.L��5�	��g8����M-�S��5�q��ϣ,�8��i���j�c���i���i���ɭ\\A���\Z +��$]O2�L���iWZD��GP�T+�o�����!��`|l\r�<��`������$�}��FsL��d.d���V�2�U�	�9��#:PVQT1O�R�s:�)�����$+�é(�jP�j�W���֪{����\"��������f�zX���X���+��!�r�V�T��%��Ժ�}B^% �y�U�A���L�&oʰ��U���B�,EBQ}o�v-ny�0v���`�>k�i��W�H˻�z�\r�xt�C��D�.b��ܓ��K�!ϖ���1iڏJn��+�_(��n��:�B�mU-׭��n��iWCa�AXv�{�|l�����i��5�l\\���kWF,,2yUR�#�W)�������<�*�F���\n�j�Wp:�z��{�B��k�^�w��c�:�`�U��[/xe�/8^I���(ԜJ���Z���z��g ��dA�8K^��\0\0��\0PK\0\0\0\0\0!\0��\0\0�\n\0\0\0\0\0word/settings.xml�VYo�8~_`����ut�H�:����-�mQe\0%�6^ )+�ߡ(Fu���$r�o�s���\'F\'\'�4|�WQ0��5�U��C>�&� ^#*8^g���������46�� ��X�\n���,uu��+!1�^(�\\�!dH=6rZ	&�!%�Ĝ�$�A#VA�x�CL���bo�J&�{R���5���u�w�j榳*L����H���E�\'=��G��rm�%�?��~�����T��ZC�u�e��g�x�\n�9�W���-��Qw\Z<����H�]�I��ri��^�*�x�B��BQ��,����&���Ī�$A9FQZ<F�����Ү>+����A!��\n�ө�5�<��0B��	��ˤ���H��`UHT�Vp��r��[�-T�� :\'�B.���K�\n�^��R�\'w��A��b^@���%��\\(����S���8bUG�{l\'jl#�(�*q?L�U���b:nH��P��j�s�8�`�^��S�\r���:�<x�̭��0m�����?�*#�D�RB}�5��\Z��	ø�6����Χ�d������rN-־]^p�z�\'�g3ۦwchi��7�1�l����(g����g�Iw�^��=�E�]�9��ؤ���(�.I��g�׳Q�e�lo�1��M�,GѶ��u2\Z�<�\'˹E���`CYfW��|w��1a���������.�bY�7�{~�ay��9ESz�t��!Js�f�э��D�;��`��À�K�Q*L�O�Xvc�A�F:k�B��7�f=��0O�MYx-�;V���\'e�!<mf࿡��|mc>�Ч�����x����\r\"�!^��&�\ro�V�?Fw)I�K:�,���ʾ���pG��-��t��ur��6���@[x\Z���&����c�-}/(-����U��d���$�����%�_wzr���N\\�n��=�U�t\r�KSt����HVX^P\'52ԻT](w%�6�qE��3+�ez��D�KػF(xr�Z�萇���\0\0��\0PK\0\0\0\0\0!\0{�P}W\0\0�\0\0\0\0\0word/fontTable.xmlԖ]o�0��\'�?D�/qB��\neE�4�b�kcb��mH��;���NTېF	����{N���\"�1c�VS��0���z��f��=-oF(���5Z�)�3��g���U�\\+g#���D�)*�+\'qli�$�=]2��6�8�j6�$�Ƕ��Z�������!jm�%.:�9e4�J�\\=?6L��V���ܪK�*m֥єY{��񓄫�M��IN��:w=�Lܬ(�V0=���(�t�q��!+�$C�\\TM� ~�˕�^�-K`hG��	���x��E�7�1��Íi#�Dr��T�%Q�@�-:}G��i�,���֮0�`�B��@^*��=��\n�}F\'�@��3,?n�s�Kf�Ϭ���+�7�J$\nC��S���D��!�O������Q��ʑȸU�D��\'���Dzk83�I0)䢏���g#&o�!���P@r�����x�E�\Z,�Cq��d�$]���p.��B�N7���� ���H񲎂�D��0�`�؊[�&>8=-�����+��ȸ.��d�,�:S�\0�\"��wǿ�lW)�6�\'�)ܫ��i�V.�u.O\Z��4���f�8�П�}���O\0\0\0��\0PK\0\0\0\0\0!\0�ϛ\' \n\0\0�J\0\0\0\0\0word/styles.xml�\\�r�6}�L��ç���u�T{�t�j<��i�L�)�0&	��\";_�ł�xą��2Ӽ��=���YP��ǟ����̒��x�_\\��}��a�~�_\\��:i�Ł����g��?������i���qz�sw�e��� �7,��b�b��I�ep�<\"/y�m/|m���xȳ����r��f��^s���.bq��	����\rߦ��=��^$�6>KSXt*{��cmf8i����T�������@����K�+\n]\'��b�x���N�W�@�o��ۅY*/�I~�_��B�Y�쯽���\\\n\"����)w����&��ћ���?�J�^���~���p�FE˭�A�-�⇢����.�d�Bӧ�lZ�ݹ�%�il��,>K��VW8���C0�����b8b�C.98zyU\\|�I�z�L� h\0��f��q�\n0g�w����Y�����E,h�t�!�\"���+Ą�%��[L�9�1�����a����H�ܢ/vqӟΐa\Z��䳭�-��=��r\0�Q��	��a6������Cã(�����O�w��FrE��]������t7�	����u7��u�%V҃�	_��̉��	��\r��h��uD�#�#\Z�h�`@�F�[G4��:�Γ#|��΢1z����y29��\0\r;J��h�^�=$�v���Z��)�\\�Vm�(���2KD�����r랭�?Gۍ�r8%��~����������\n�vtMx09��>���6\"X�ܳ\'цO���g�N����w�a�9�\r��V0u�iF׼e�O�\'7�԰�6�N\r�4��|�!�F�J�-�\\��)�v�Ė�9�\0e	*]�/�毒��}c��U*:�>a�*q�i�q:��J�Z���ӼnE(��.,�@�<̬w���-�zk�$��Y���|:7�On�Z�⠣(��P(���k�JM��+�P\rkd��Mk-��E�#���wb��\0UZ�5[����8��п�D�~�4��r��%)shhc�Σ���\r��%>2uˀ@�R���漥s\"�{r����e��p��yf��\Z�.��7	�/��5s��7	(�j�M�utj�L�MVoy��e�\Z��5�fQ�y��ś��~ě\0ԏx��oPw�n�O�	X�ڠ5�,� �b�U�*�7�Z����y��~��A�	(�j�7�::&�&`a&԰����oP?�M\0�G�	@��7��&\0u�v��ě�e�\rZS��M\0��\rTov�ц�⍻���7�:@M�&�XG�&���J��P\rK�7�ؐ!�Br�,��&���&\0�#��~ě\0�]��A�o��6hM-�7�Z4PY�	@��pT�q3~u�&�X�)����U��:@5,-�,�Kg�&\0a�s�lVԏxVԏx��oPw�n�O�	X�ڠ5�,� ky�@e�&\0Yk�Q��=��ś�b��xP��ST-�,�\0հ����o��x���@��l�ԏxVԏx���w;H�M�����e�&\0Y˃*�7�Z�{��(��ԡ�����\ZȀ#C����?�5K�Ȋ����X����%��ѡ��=6��W!�J�3��S*D�NT��v�U0�qH��7P=T.��$Y8�̞�P��-�,�֠H�u�%@X\"wAyY�,�|�#U�����9*�\r�xA��ㆣ�b�Vd*��\":	�H�pʋ�Ry1|T�����U¥����҈Ue�?���<�B��l�|�˙���	-nӎ����ʮ*J�&�+\n�~�uS\rG²������K��C�I�\'�:9��+����\n������T�]�3��cS���)��D��\Z����e�ϻ�a1�q��PJ(=]�M�UQ�!�����<��3�З��P��Oy�T+�$�R���ʵ�O1�V���Il�!����҂a����;,��o9C=.�ɍ�.�����U�NV�7�<�5\'�5\'���.F���m�!��*T�?�b)/P�LS2<y\n�߲0��C*ebk�\Z�u��/��W3�Y&\"��K#p&����\'�.�\"�/���]�b	�6���{!�LX�YV��v�\0�y�<�\n��]\n�Y�LROѪ�7��L�������>���F#��\rs*�Ok� �47��tHXpH�_|z��0����[��f	��K��hkÃ�Es�;fOTf�e�5�單��1�fjW�@�7J��`��o��;����L���QS���h����oL���3t�kzOׇ3¹����y�m�� b�͉���\Z�8�Z�Y�L[Ȗ�2\'�#Ec���}�}l���\08����1��;wgP�	\n���WI?z�.D�=jU�8��Vi�y���R��P�}*�v�r�Pod�/\\���1w׍��w��ɟ��3���Y:\'s2�L�ۨ�c�*s���Yr��fߵJ�h:����ʵ���#�Y,^VS�C�V������(��u胧4y��.W�#|���G�?�����ӌ%aOh�r?:�u���8I�F�P��HFU���o8)����CMޮU�\ro���y�_U��oKN��V���[e���\"{���\0\0��\0PK\0\0\0\0\0!\0��qE�\n\0\0�M\0\0\Z\0\0\0word/stylesWithEffects.xml�\\Ks�8�o��O��z8��5ʔ�We2�ȩ9S$d�L\\������h�_\"����E&��/|\r*l���K9�,�C�,�雉���A�<.��wg�p�����x�+�ݟ���/?����5b���z��Kw[���y�oY��o���x�7����|�	}v��Yp>�L\'�W�q��9��zɳ��J\\ܖ�S�\0ֆg�W�ox�x{��.=�W��0\n�W�=Y�b���eɵR�L+$�\\K��G�\"kYс+W~��.fI����@���0=�q�40q[��|̈�8*����EO�L�����C([�:��Eq$� �{�jS�tr�!B�@Q��Yj{a�Ŝ暪sa?��ߥZ�4&�>yҲĶ��l���W5-��ں���2׉���Ǆg�:���Gd���\"����vQ����k�.�~��ȝ����a�\0R�~�I�Ѕ;�ˋ�<�:onŬ�;~^T����=��� �ً��lV��\n\rjc��<�c,9�����҅��+1��K���V7B�9�Y~V�Mk����z>�<�m\n$4]\0ﯣPDw��������vW (\0��b��q�&`��dl��6���ĂU7�.b�����Y�3�ѥ{��0�bq�)&\n��CLL�a��ز�{΂���wH�J��wI�/.1�<���g��I�x\"�_��0Gڅm�@�YBNe;Q��%�A���ջ�@3aQ�\0�k��|����\"��x�/.�����P-dnT��Ԃ�2��91�:��bE+�zW���wE+GzW�R�wE+zW�޻����p]�{H\\�,��7H�!,\"��=L7Hud�u�z���y������1�\\��MU����rUd\\7{<�Ylݓ9��8�zy��>���G�c��\nR��&<�t�����-��9�EF����/�Y�SF�r��9|��\nE����vt͖H���}p��/��	\'�pa�K��_Y���5���B�E���q]�f���� ˅�	(���,.��E�)��Rt�|���p�(��x|���|�␶ץ�0�;��G<��r��å��4�7��O\"�K�\\�O����ɍ��ֱ8��u8$\nn6�-�Ai����\"�\05�fXø�Țt���P|	l[���Y�w;�\r�������g虁�(�	|]�3��67�<*Z���$Ӱ�g�L�*�аRhd�s��5�2�8Z`YӲ�b����|i��\ZȮ�T7	�/��5�B�nP�Ԯ���4j������&�P5�1�r��Q�u�\n�ɛ`�8�M\0\Z��	@�7h8y���G�,knМZ%oN���EUɛ\0d�\r���wFe�C)�nG o�u���M@�����	X8�&\ZX��X�7h�&\0�C��qț\04y���w?�x�M���ͩU�&\0YӃ��7��pC\'y����M@�P��	(��i�>�������M��)6ɠ�0�m�\Z��	�C��qț\04y���w?�x�M���ͩU�&\0YӃ��7Ț:�7�\'o�u���M@��N�P5�������M��|L� �r*��E�7��qț\04y���w?�x�M���ͩU�&\0YӃ��7Ț:���\'o�u���M@��N�P5y�����TG�\Z��	@���ɛ\0�SN\0�]d�qț`�8�M\0\ZN�� �7˚4�Vɛ\0dM\Z�J� kn�������S��$��gP��@��DT~c�AW!�;d `i��!=�&���ɡ��=7$*\\G!�W�_�-�J#���H\'��o��\'�\0�Z�)U�����B؞$\Z�@��5����|�\\H�f �ץZ��\'�\Z�T[�X,�|`\"6U�a�[��C�iP΁���l~���\Z���S���Kܟ⢻AJ5c�G��l��zQ��B!��.�\rbWYmćn��0���/l�|㱗M+�^��iG���>���ʖ�釵�V�����#��r�w��2���pR�Q]\'\'�͠�`* /o�!-�o��L(���,�jORѻС�Go�w����9*��+g���VB��fXo�P6��ڌT���1}�zj	5�ҋ�^A���J��\Z��\n��%�1]O,�?H����($6�j��o�a�Gs��A��vn����Ӄ�W{;Y�o��B�@�{$����S�ɫ��OC �u$���O��U_����œ p��Eѯ�R�S�Ԉm\nyw:��_CԚ���3l�@M�\0�T��������d�Y�\Z-y�3�`V:Bpܐ\n�\0�w�<m֭���.׬D%i�\Zi5�W�t.��5Ȱs��]�h�,y�\\���\ZHC���t(XpȄ_[��I��ɨ�# N��z^d�>GLd���і��T���X=���m6�[�6)��6�v��}#y��n�F�q���.�i�p�dԦ�r/Zy�ut�7�n���Z�����pj}>�m^�F~o$�Y)Xp&NI��ݳ0Cyb�:��MbQ����)>}nw�x<��\0Y?C�ߢ�.����2����ʏޣw<����jV���.+́0o߾R\n�c|RէFnWo/������t,�M�B�W����8�� ��$�5N�M�m�p���9�,�,;Tv��z�o���,Tr)��(��|�Ƌn*y�N9<�]����s�&ΰ8�j2�gQpV)O=�����`��.LO3�	{�S���i��[c��I2m����bT�ժ�\r\'�������v�n�M�JI��[�B�F�G��f��[C�VJ�_�۲z���\0\0��\0PK\0\0\0\0\0!\0/��p�\0\0�\0\0\0docProps/app.xml �(�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�S�n�0��?�7r���YŐb�a[�mϚL;�dI�ؠ�׏�W�zjN�����#ͯ�;� D�l��gE��U�ֶ-�����*�\"J[K�,��b~->~���<�3������~�XT{�d�Q�R�q��Heh�k\Z��Ʃ�,�ˢ�b��`k�/�$�����W�v����\'ÂW�y#��ގ��;�&�W��tb^?U|+[�9\"��BMħ�+�F�7{�B\nQ,��%g	��zo��H��Z]��ݐD�p��pJg�)h<\n������d�j�و�]�m�~O��OR�6�h����+�oA��JM���P�B�Z�e������� ��)�~l,l|� *����7�L�R�b>8�F�8w7��\Z�o���yjv�0ZM�$pz�Ս뼴Gz|B���x�+wӟ�K��d��G�������X��@��;��i�\'�W��R���������������oV���~��8���c\0\0��\0PK\0\0\0\0\0!\0H�Q�\0\0�\0\0\0docProps/core.xml �(�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0|��N�0��H�C�{�dP@��I��$$�@�Bb�@�D����IۭP�����/��,V�u�|�ښ%�3F0�*m6Kr_��3�F��\ZX�Y��ri=�z�����D�	�tK�Et9�An�a&_����o��Ml��;�5�P퀩�d�TrD�w_�\0%)TP��@���o-��ß\r}凲�غ��nܟl%���z6M3k��1���>�\\�����t^I �B�5VP,��1����+H�c҃@��>CreU��o��;�ߠm�W!vO�خ H��wؓDTW\"�M|�\r�-��j��[Ğ���]��Cw�8�c��f�Dw���}����\\�b��qʲt>/Y�g<g�[k�߹5$�݀�����<�)q����\0\0��\0PK\0\0\0\0\0!\0���Ǵ\0\0�\0\0\0\0\0word/webSettings.xml�UMO�0�#�\"ߩ�ҦMD�T!�J!>~��8����l����&|�e��Z=e<3�yf��9;�mt��>(�%a��$�\n��]�����dJ����+K����ώ��ڢ�ս�3C�,6F�d�+(\rb)\rp�b�oxģ_P���ʝ0�GU)���4#�4�;,�4J�+#m���K��`�R����~��_;B�������;\r}!2Jx��6C���\n�,�,�IbD�ka��J�[6\"3_��������Kr��l<���.^A��Pk���Fi�f��e߼��N-�q?���;�����z���?0E\'��K�O\r�6��4�V|�/C�T���T�~X���>Pډ�5ݛ��ȳ�l<FEr����l2\ZN�|�Mz�=�,�26���A���G�������z����� }�/�����\\��wV��\0\0��\0PK\0\0\0\0\0!\0�Y;3@\n\0\0Xz\0\0\0\0\0word/numbering.xml�]ݎ�8�_i�!���N�\0��T�R�ΪG����^�5I�\n\Z~\" ������G�ǚW�c��͏�IS#n:]�>���������_=w�j���Cu�������0���͇�pŖ������ov4���w����O���������{����L�����h�9�0���x��I������9�MQ���0��Qt֖�jEÔ�GS���zBϊ�q�L<+��t�\0ԏV�l׉߀�bfd���)��i�>\\:��,q�ҟ�EHq�x/n��N����\'��B?:8�+�Ԁ�C֥�2&^=7{�|Tu�}���Sh�a(�)ra�q#��r@�{�[��R�L:\"�ĥu��g��r�1ѐ��h�����ҝ�ӌ�g��-d�=S���H�\".���|���p�햟_� ��.�����Ï0[X�(�]�������*�#~���ޫ��L���l�\'��wrc��j�_ߎv���m:��{.�������=�Y��j�	�q_�\r~���хIF�+EQԤ0ׅq�\\��`��x��ۓ����W��˭?��������k?���\"n��.?gZғ��$S��T��y�>�6�&���Y�������`���ar�]p| ���-�LJ,�D�w�R*%�ir�5��^a%H&�����b�Y�б��O��������ͥ��4Jj�|�����r�TP ��\n�_P?����uѩ�5>a%�P������i󹘀�Y�N�N,f1��\Z��n�6�8l_�Bu�������.,��5>��K-i]�8C���Y܌M,Θ	��7��\"����-\\���ř����.�E\0�J\\��S�>��i�\Z^�ٸ6�Z�hAe���ju�0D�Eݹ~�A�]�oO�:@d�,�\0����f���~�zv�L%��Lj�L>�L0p�$ۅ��*F�R�ZW�5���b�#��i��$�\"P]\"RQ��?a�c�jiǋE*����	�� �b����j)������T�����%�H�WP\"RQ��}�b��\"M�b��џ�6|NC�\"������y����S�/z����Fb�Ǒd�j\Z���{���O�M���0����0����0����0����0����0$���ØR��|�Lg�9�A�sյ9�٣��lR*dEJ���ksP��k��r$�N����y˾6Տ�仰��XѤ��}mNa� ��	G�ojڪ���T�o�ə�!�r��ks��DĨ?�ks�h�����\n���Pk��6����9ӵ93�����q�d�ל�s�JOyy���!X�y�e���v�	Vx��\"y�4KW��t!X�)��Yt�����w���[C��e[\\W���\nN��,���֜��w���ۙ!8�7�8N`kP[]Y<��S\nI�۲�c6_+��\Z|�ھ�<�ʒ�$���jő*�\"�i�9��~u<;B�k�x|����ye���\Z~Q���\n��Z����זwD�﹖�#\"|ϵ��{���qmxGD��7)�o�+e�1J#�e�_���j13Ł���9��n���]�H_�l�R��|n�;;�(I�����wM�\r�;;!W<��7��^C��f�3U����R�k��$�=��jȞ!������n4dϔ�^CD�fO�\rٛ�`�!Zc�7KV���ͥ��I�ٛ�`o!���(\'c~yR�xxɔ�antm��جTt�̕�n��?º�+���z���4��.�\\l��	4������f���l�_�8����A�z���@�}��Y�R��#\"\"��W�§���r1E��7ף\n��F��{T�$�F�%	����6;T�����)]=�@�TU9�f��ʶ�tL�P�tT�����y�V��4 G�*C�!*�ٞ�Ճ$KU�s6KTb�5�c\"�r����J��,�����l���4ȟJ���i�`~�Q/BT:��[�ی(���nȠds�w��*\\��zb�V��0a�eo�ƴ�h��..Z`�\"�ݖ-�˄+���R`�v롙��m)�����N��>Z`�	�x���B�O�&S����8nR�n�=�3�^��g��h~���U�n\"AtE�#��~�����_���̗��D_^�LB��:�R��QI�I.����)�!5<��,�x)gy<�^4�&׷\'\\�k\'P�ǵP>�����\Z-�(�B��:��G���Wu*��\\ Be5�o؅95���뚃�z�`<��Z>�]?�^n����A\\2������p{�H�b��a<ZtQ!8#R*}T�L1U�4Rh��2��b����A+�@�Uj�.�ك$P�]�6#�T��^�)�~3xDn O�[�$�-5M�b\'��w�(�>x�0��Ȓx�v�F��|�2�n�W:�}��t��9UU��Vf�<�<U��#~en-x�,|�/:�熈��gt<`4H|XPx%�r�p���,A�Ͱ��l���ۨC��L`e�۰��|[�(h�}#f�$�S���f���b�ov��f8��l��ɂ^���.q܋ڕ(����\'��b�C2�� q��2q��VZy��y�B$0�{�T�W*DĨ\"�Ҁ\n�h@��4�Bx�\r��h-*���_|\"���\0\0��\0PK-\0\0\0\0\0\0!\0aDե\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0[Content_Types].xmlPK-\0\0\0\0\0\0!\0�\Z��\0\0\0N\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\0\0_rels/.relsPK-\0\0\0\0\0\0!\0�_��\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0word/_rels/document.xml.relsPK-\0\0\0\0\0\0!\0cuZ��*\0\0]D\0\0\0\0\0\0\0\0\0\0\0\0\0\0�	\0\0word/document.xmlPK-\0\0\0\0\0\0!\0ȹ+3�\0\0F\0\0\0\0\0\0\0\0\0\0\0\0\0\0\05\0\0word/endnotes.xmlPK-\0\0\0\0\0\0!\0#����\0\0L\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0(7\0\0word/footnotes.xmlPK-\0\0\0\0\0\0!\0��\r!�\0\0X\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0N9\0\0word/theme/theme1.xmlPK-\0\0\0\0\0\0!\0��\0\0�\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\09@\0\0word/settings.xmlPK-\0\0\0\0\0\0!\0{�P}W\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0jD\0\0word/fontTable.xmlPK-\0\0\0\0\0\0!\0�ϛ\' \n\0\0�J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�F\0\0word/styles.xmlPK-\0\0\0\0\0\0!\0��qE�\n\0\0�M\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0>Q\0\0word/stylesWithEffects.xmlPK-\0\0\0\0\0\0!\0/��p�\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\\\0\0docProps/app.xmlPK-\0\0\0\0\0\0!\0H�Q�\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\04_\0\0docProps/core.xmlPK-\0\0\0\0\0\0!\0���Ǵ\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�a\0\0word/webSettings.xmlPK-\0\0\0\0\0\0!\0�Y;3@\n\0\0Xz\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�c\0\0word/numbering.xmlPK\0\0\0\0\0\0�\0\0Hn\0\0\0\0','2015-02-09 17:53:24',11);
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignor_id` int(11) NOT NULL,
  `assignee_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings`
--

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
INSERT INTO `meetings` VALUES (2,9,7,'please come to meeting..','2015-02-09 15:00:00','2015-02-09 16:00:00',1);
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receiver_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (3,9,6,'hi navinaa'),(6,9,6,'lololo naivn..');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin'),(3,'Second marker'),(4,'Student'),(2,'Supervisor');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_activations`
--

DROP TABLE IF EXISTS `user_activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_activations` (
  `user_id` int(11) NOT NULL,
  `activation_code` varchar(5) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `activation_code_UNIQUE` (`activation_code`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_activations`
--

LOCK TABLES `user_activations` WRITE;
/*!40000 ALTER TABLE `user_activations` DISABLE KEYS */;
INSERT INTO `user_activations` VALUES (11,'cetPW',1),(5,'cmMNW',1),(8,'fgrTX',1),(7,'fikBQ',1),(9,'fpO59',1),(6,'jprvB',1),(10,'vyBLT',1);
/*!40000 ALTER TABLE `user_activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` char(40) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `role_id_idx` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (5,'Roshan Tamil Sellvan','t3es','icemission@gmail.com','ebbdd5365d50d9eebb597feafafa65bcaefb1068',2),(6,'James','james','t3es92@gmail.com','917ffaf0b1101ef1c2621fc42f591f47ad41dccc',4),(7,'Thomas','thomas','thomas@gmail.com','c03555c8289418493aeb1eefc743b450b718a9a1',4),(8,'John','john','john@gmail.com','31f51faebeaafcb546721a7bd012db57b5434992',3),(9,'Navin','navin','navin@gmail.com','806920248bd7d42b957124399b9964b32891356d',2),(10,'Tim','tim','tim@gmail.com','b7b2e7135777a53ab1f1a0b0abb0dd2bcd5060c9',3),(11,'admin','admin','admin@gmail.com','f865b53623b121fd34ee5426c792e5c33af8c227',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'esupervision'
--

--
-- Dumping routines for database 'esupervision'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-09 18:27:42
