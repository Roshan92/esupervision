<?php
include('master.html');
// Check if a file has been uploaded

if (isset($_COOKIE['username']) == false){
        header( "refresh:0.0000001;url=loggedOut.html" );
    }

$user_id = get_user_id($_COOKIE['username']);

if(isset($_FILES['uploaded_file'])) {
    // Make sure the file was sent without errors
    if($_FILES['uploaded_file']['error'] == 0) {
        // Connect to the database
        $dbLink = new mysqli('localhost', 'root', '', 'esupervision');
        if(mysqli_connect_errno()) {
            die("MySQL connection failed: ". mysqli_connect_error());
        }

        // Gather all required data
        $name = $dbLink->real_escape_string($_FILES['uploaded_file']['name']);
        $mime = $dbLink->real_escape_string($_FILES['uploaded_file']['type']);
        $data = $dbLink->real_escape_string(file_get_contents($_FILES  ['uploaded_file']['tmp_name']));
        $size = intval($_FILES['uploaded_file']['size']);

        // Create the SQL query
        $query = "
            INSERT INTO `document` (
                `name`, `mime`, `size`, `data`, `created`, `user_id`
            )
            VALUES (
                '{$name}', '{$mime}', {$size}, '{$data}', NOW(), '{$user_id}'
            )";

        // Execute the query
        $result = $dbLink->query($query);

        // Check if it was successfull
        if($result) {
            echo '<br><br><br><br><div class="alert alert-success alert-dismissible" role=\"alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button><p>Your file was successfully added!</p>
                </div>';

            $log = "You uploaded a document named, {$name}";

            $sql = "INSERT INTO `activity_log` (`log`, `user_id`, `date`) VALUES ('" . $log . "', '" . $user_id . "', NOW())";
            mysql_query($sql) or die ("error in function_name: <br>Sql: " . $sql . "<br>" . mysql_error());
        }
        else {
            echo '<br><br><br><br><div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button><p>Error! Failed to insert the file</p>
                </div>'. "<pre>{$dbLink->error}</pre>";

        }
    }
    else {
        echo '<br><br><br><br><div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button><p>An error accured while the file was being uploaded. </p>
                </div>'. 'Error code: '. intval($_FILES['uploaded_file']['error']);
    }

    // Close the mysql connection
    $dbLink->close();
}
else {
    echo '<br><br><br><br><div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button><p>Error! A file was not sent! </p>
        </div>';
}


// Echo a link back to the main page
echo '<div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button><p>Click <a href="upload_document.html">here</a> to go back </p>
    </div>';
?>

